(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 5.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     11000,        315]*)
(*NotebookOutlinePosition[     11748,        341]*)
(*  CellTagsIndexPosition[     11704,        337]*)
(*WindowFrame->Normal*)



Notebook[{

Cell[CellGroupData[{
Cell["Incremental mean and variance", "Subtitle"],

Cell[CellGroupData[{

Cell["Incremental mean", "Section"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`x\&_\_n \[Congruent] \(1\/n\) \(\[Sum]\+\(k = 1\)\%n x\
\_k\) \[LongEqual] \(\(n - 1\)\/n\) \(1\/\(n - 
                1\)\) \(\[Sum]\+\(k = 1\)\%\(n - 1\)x\_k\) + 
        x\_n\/n \[LongEqual] \(\((n - 1)\) x\&_\_\(n - 1\) + x\_n\)\/n\)]]], \
"Text"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Incremental variance", "Section"],

Cell[TextData[Cell[BoxData[{
    \(TraditionalForm\`\(\[Sigma]\_\(x\[VeryThinSpace]y\)\^2\)\_n \
\[Congruent] \(1\/\(n - 
              1\)\) \(\[Sum]\+\(k = 1\)\%n\((x\_k - x\&_\_n)\) \((y\_k - 
                y\&_\_n)\)\)\), "\[IndentingNewLine]", 
    \(TraditionalForm\`\(\(\[LongEqual]\)\(\(1\/\(n - 
                1\)\) \(\[Sum]\+\(k = 1\)\%\(n - 1\)\((x\_k - 
                  x\&_\_n)\) \((y\_k - y\&_\_n)\)\) + \(1\/\(n - 
                1\)\) \((x\_n - x\&_\_n)\) \((y\_n - 
              y\&_\_n)\)\)\)\), "\[IndentingNewLine]", 
    \(TraditionalForm\`\(\(\[LongEqual]\)\(\(1\/\(n - 
                1\)\) \(\[Sum]\+\(k = 1\)\%\(n - 1\)\((x\_k - 
                  x\&_\_\(n - 1\) + x\&_\_\(n - 1\) - x\&_\_n)\) \((y\_k - 
                  y\&_\_\(n - 1\) + y\&_\_\(n - 1\) - 
                  y\&_\_n)\)\) + \(1\/\(n - 1\)\) \((x\_n - 
              x\&_\_n)\) \((y\_n - y\&_\_n)\)\)\)\)}]]], "Text"],

Cell[TextData[Cell[BoxData[{
    \(TraditionalForm\`\(1\/\(n - 
            1\)\) \(\[Sum]\+\(k = 1\)\%\(n - 1\)\((x\_k - x\&_\_\(n - 1\) + 
              x\&_\_\(n - 1\) - x\&_\_n)\) \((y\_k - y\&_\_\(n - 1\) + 
              y\&_\_\(n - 1\) - y\&_\_n)\)\)\), "\[IndentingNewLine]", 
    \(TraditionalForm\`\(\(\[LongEqual]\)\(\(1\/\(n - 
                1\)\) \(\[Sum]\+\(k = 1\)\%\(n - 1\)\((x\_k - 
                  x\&_\_\(n - 1\))\) \((y\_k - 
                  y\&_\_\(n - 1\))\)\) + \(1\/\(n - 
                1\)\) \(\[Sum]\+\(k = 1\)\%\(n - 1\)\((x\&_\_\(n - 1\) - 
                  x\&_\_n)\) \((y\&_\_\(n - 1\) - y\&_\_n)\)\) + \(1\/\(n - 
                1\)\) \(\[Sum]\+\(k = 1\)\%\(n - 1\)\((x\_k - 
                  x\&_\_\(n - 1\))\) \((y\&_\_\(n - 1\) - 
                  y\&_\_n)\)\) + \(1\/\(n - 
                1\)\) \(\[Sum]\+\(k = 1\)\%\(n - 1\)\((x\&_\_\(n - 1\) - 
                  x\&_\_n)\) \((y\_k - 
                  y\&_\_\(n - 1\))\)\)\)\)\), "\[IndentingNewLine]", 
    \(TraditionalForm\`\(\(\[LongEqual]\)\(\(\(n - 2\)\/\(n - 
                1\)\) \(1\/\(n - 
                2\)\) \(\[Sum]\+\(k = 1\)\%\(n - 1\)\((x\_k - 
                  x\&_\_\(n - 1\))\) \((y\_k - 
                  y\&_\_\(n - 1\))\)\) + \((x\&_\_\(n - 1\) - 
              x\&_\_n)\) \((y\&_\_\(n - 1\) - 
              y\&_\_n)\) + \(\(y\&_\_\(n - 1\) - y\&_\_n\)\/\(n - 
                1\)\) \(\[Sum]\+\(k = 1\)\%\(n - 1\)\((x\_k - 
                x\&_\_\(n - 1\))\)\) + \(\(x\&_\_\(n - 1\) - x\&_\_n\)\/\(n - 
                1\)\) \(\[Sum]\+\(k = 1\)\%\(n - 1\)\((y\_k - 
                y\&_\_\(n - 1\))\)\)\)\)\), "\[IndentingNewLine]", 
    \(TraditionalForm\`\(\(\[LongEqual]\)\(\(\(n - 2\)\/\(n - 
                1\)\) \(\[Sigma]\_\(x\[InvisibleComma]y\)\^2\)\_\(n - 1\) + \
\((x\&_\_\(n - 1\) - x\&_\_n)\) \((y\&_\_\(n - 1\) - y\&_\_n)\) + 0 + 
        0\)\)\)}]]], "SmallText"],

Cell[TextData[Cell[BoxData[{
    \(TraditionalForm\`\(\[Sigma]\_\(x\[VeryThinSpace]y\)\^2\)\_n \
\[LongEqual] \(\(n - 2\)\/\(n - 
                1\)\) \(\[Sigma]\_\(x\[VeryThinSpace]y\)\^2\)\_\(n - 1\) + \
\((x\&_\_\(n - 1\) - x\&_\_n)\) \((y\&_\_\(n - 1\) - 
              y\&_\_n)\) + \(1\/\(n - 1\)\) \((x\_n - x\&_\_n)\) \((y\_n - 
              y\&_\_n)\)\), "\[IndentingNewLine]", 
    \(TraditionalForm\`\(\(\[LongEqual]\)\(\(\((n - 2)\) \(\[Sigma]\_\(x\
\[VeryThinSpace]y\)\^2\)\_\(n - 1\) + \((x\_n - x\&_\_n)\) \((y\_n - y\&_\_n)\
\)\)\/\(n - 1\) + \((x\&_\_\(n - 1\) - x\&_\_n)\) \((y\&_\_\(n - 1\) - 
              y\&_\_n)\)\)\)\)}]]], "Text"],

Cell[CellGroupData[{

Cell[TextData[{
  "Starting condition: ",
  Cell[BoxData[
      \(TraditionalForm\`\(\[Sigma]\^2\)\_1\)]],
  " is undefined"
}], "Subsection"],

Cell[TextData[{
  "but the equation for ",
  Cell[BoxData[
      \(TraditionalForm\`\(\[Sigma]\^2\)\_2\)]],
  " yields the correct result for any finite ",
  Cell[BoxData[
      \(TraditionalForm\`\(\[Sigma]\^2\)\_1\)]],
  ":"
}], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`\(\[Sigma]\^2\)\_2 \[LongEqual] \((x\_2 - \
x\&_\_2)\)\^2 + \((x\_1 - x\&_\_2)\)\^2\( \[LongEqual] \&! \)\(0 \( \
\[Sigma]\^2\)\_1 + \((x\_2 - x\&_\_2)\)\^2\)\/1 + \((x\&_\_1 - \
x\&_\_2)\)\^2\)]]], "Text"],

Cell["so we set", "Text"],

Cell[TextData[{
  Cell[BoxData[
      \(TraditionalForm\`\(\[Sigma]\^2\)\_1 \[Congruent] 0\)]],
  "."
}], "Text"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Incremental mean and variance as filters with constant coefficients\
\>", "Section"],

Cell[TextData[{
  "If ",
  Cell[BoxData[
      \(TraditionalForm\`n\)]],
  " gets very large, the mean and variance become very insensitive to new \
data. This can be avoided by limiting the sample index ",
  Cell[BoxData[
      \(TraditionalForm\`n\)]],
  " to be ",
  Cell[BoxData[
      \(TraditionalForm\`n \[LessEqual] N\)]],
  ". "
}], "Text"],

Cell[TextData[{
  "If we substitute ",
  Cell[BoxData[
      \(TraditionalForm\`n\)]],
  " with ",
  Cell[BoxData[
      \(TraditionalForm\`N < n\)]],
  ", we get"
}], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`x\&_\_n \[TildeTilde] \(\((N - 1)\) x\&_\_\(n - 1\) + \
x\_n\)\/N\)]]], "Text"],

Cell["and", "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`\(\[Sigma]\_\(x\[InvisibleComma]y\)\^2\)\_n \
\[TildeTilde] \(\((N - 2)\) \(\[Sigma]\_\(x\[VeryThinSpace]y\)\^2\)\_\(n - \
1\) + \((x\_n - x\&_\_n)\) \((y\_n - y\&_\_n)\)\)\/\(N - 1\) + \((x\&_\_\(n - \
1\) - x\&_\_n)\) \((y\&_\_\(n - 1\) - y\&_\_n)\)\)]]], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`\((x\&_\_\(n - 1\) - x\&_\_n)\) \((y\&_\_\(n - 1\) - 
            y\&_\_n)\) \[LongEqual] \((x\&_\_\(n - 1\) - \(\((N - 1)\) x\&_\_\
\(n - 1\) + x\_n\)\/N)\) \((y\&_\_\(n - 1\) - \(\((N - 1)\) y\&_\_\(n - 1\) + \
y\_n\)\/N)\) \[LongEqual] \((\(x\&_\_\(n - 1\) - x\_n\)\/N)\) \((\(y\&_\_\(n \
- 1\) - y\_n\)\/N)\)\)]]], "SmallText"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`\(\[Sigma]\_\(x\[InvisibleComma]y\)\^2\)\_n \
\[TildeTilde] \(\((N - 2)\) \(\[Sigma]\_\(x\[VeryThinSpace]y\)\^2\)\_\(n - \
1\) + \((x\_n - x\&_\_n)\) \((y\_n - y\&_\_n)\)\)\/\(N - 1\) + \((\(x\&_\_\(n \
- 1\) - x\_n\)\/N)\) \((\(y\&_\_\(n - 1\) - y\_n\)\/N)\)\)]]], "Text"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Iterative algorithm with minimal number of computations", "Section",
  PageBreakAbove->True],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`x\&_\_n \[Congruent] \(1\/n\) \(\[Sum]\+\(k = 1\)\%n x\
\_k\) \[LongEqual] \(\(n - 1\)\/n\) \(1\/\(n - 
                1\)\) \(\[Sum]\+\(k = 1\)\%\(n - 1\)x\_k\) + 
        x\_n\/n \[LongEqual] \(\((n - 1)\) x\&_\_\(n - 1\) + x\_n\)\/n\)]]], \
"Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`\(\[Sigma]\^2\)\_n \[LongEqual] \(\(n - 2\)\/\(n - 
                1\)\) \(\[Sigma]\^2\)\_\(n - 1\) + \((x\&_\_\(n - 1\) - \
x\&_\_n)\)\^2 + \(1\/\(n - 
                1\)\) \((x\_n - x\&_\_n)\)\^2 \[LongEqual] \(\((n - 2)\) \(\
\[Sigma]\^2\)\_\(n - 1\) + \((x\_n - x\&_\_n)\)\^2\)\/\(n - 1\) + \
\((x\&_\_\(n - 1\) - x\&_\_n)\)\^2\)]]], "Text"],

Cell[CellGroupData[{

Cell["Initialization", "Subsection"],

Cell["First pass:", "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`x\&_ \[Congruent] x\_1\)]]], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`\[Sigma]\^2 \[Congruent] 0\)]]], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`k \[Congruent] 0\)]]], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`j \[Congruent] 1\)]]], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`i \[Congruent] 2\)]]], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`c \[Congruent] 1\)]]], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`d \[Congruent] 1\/2\)]]], "Text"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Iteration", "Subsection"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`m \[Congruent] x\&_\)]]], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`x\&_ \[Congruent] d(\ j\ m + x\_i)\)]]], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`\[Sigma]\^2 \[Congruent] 
      c(k\ \[Sigma]\^2 + \((x\_i - x\&_)\)\^2) + \((m - x\&_)\)\^2\)]]], \
"Text"],

Cell[CellGroupData[{

Cell[TextData[{
  "If  ",
  Cell[BoxData[
      \(TraditionalForm\`i < N\)]],
  ":"
}], "Subsubsection"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`k \[Congruent] j\)]]], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`j \[Congruent] i\)]]], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`c \[Congruent] d\)]]], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`i \[Congruent] j + 1\)]]], "Text"],

Cell[TextData[Cell[BoxData[
    \(TraditionalForm\`d \[Congruent] 1\/i\)]]], "Text"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
FrontEndVersion->"5.2 for Microsoft Windows",
ScreenRectangle->{{0, 1400}, {0, 977}},
ShowPageBreaks->True,
WindowSize->{1051, 855},
WindowMargins->{{18, Automatic}, {Automatic, 23}},
PrintingCopies->1,
PrintingPageRange->{Automatic, Automatic},
Magnification->1
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{

Cell[CellGroupData[{
Cell[1776, 53, 49, 0, 41, "Subtitle"],

Cell[CellGroupData[{
Cell[1850, 57, 35, 0, 52, "Section"],
Cell[1888, 59, 303, 5, 26, "Text"]
}, Open  ]],

Cell[CellGroupData[{
Cell[2228, 69, 39, 0, 52, "Section"],
Cell[2270, 71, 920, 15, 59, "Text"],
Cell[3193, 88, 1909, 31, 64, "SmallText"],
Cell[5105, 121, 657, 10, 46, "Text"],

Cell[CellGroupData[{
Cell[5787, 135, 142, 5, 43, "Subsection"],
Cell[5932, 142, 238, 8, 26, "Text"],
Cell[6173, 152, 257, 4, 28, "Text"],
Cell[6433, 158, 25, 0, 26, "Text"],
Cell[6461, 160, 113, 4, 26, "Text"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{
Cell[6623, 170, 94, 2, 52, "Section"],
Cell[6720, 174, 349, 12, 41, "Text"],
Cell[7072, 188, 175, 8, 26, "Text"],
Cell[7250, 198, 130, 2, 26, "Text"],
Cell[7383, 202, 19, 0, 26, "Text"],
Cell[7405, 204, 316, 4, 28, "Text"],
Cell[7724, 210, 382, 5, 22, "SmallText"],
Cell[8109, 217, 324, 4, 28, "Text"]
}, Open  ]],

Cell[CellGroupData[{
Cell[8470, 226, 98, 1, 52, "Section",
  PageBreakAbove->True],
Cell[8571, 229, 303, 5, 26, "Text"],
Cell[8877, 236, 398, 6, 27, "Text"],

Cell[CellGroupData[{
Cell[9300, 246, 36, 0, 43, "Subsection"],
Cell[9339, 248, 27, 0, 26, "Text"],
Cell[9369, 250, 87, 1, 24, "Text"],
Cell[9459, 253, 91, 1, 24, "Text"],
Cell[9553, 256, 81, 1, 24, "Text"],
Cell[9637, 259, 81, 1, 24, "Text"],
Cell[9721, 262, 81, 1, 24, "Text"],
Cell[9805, 265, 81, 1, 24, "Text"],
Cell[9889, 268, 84, 1, 25, "Text"]
}, Open  ]],

Cell[CellGroupData[{
Cell[10010, 274, 31, 0, 43, "Subsection"],
Cell[10044, 276, 84, 1, 24, "Text"],
Cell[10131, 279, 99, 1, 24, "Text"],
Cell[10233, 282, 159, 3, 24, "Text"],

Cell[CellGroupData[{
Cell[10417, 289, 104, 5, 35, "Subsubsection"],
Cell[10524, 296, 81, 1, 24, "Text"],
Cell[10608, 299, 81, 1, 24, "Text"],
Cell[10692, 302, 81, 1, 24, "Text"],
Cell[10776, 305, 85, 1, 24, "Text"],
Cell[10864, 308, 84, 1, 25, "Text"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

